Rails.application.routes.draw do
  devise_for :users
  devise_scope :users do
    root to: "devise/sessions#new"
  end
  get 'users/profile', to: 'users#profile', as: 'user_root'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

end
